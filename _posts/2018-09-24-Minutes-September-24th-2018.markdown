---
layout: post
title: "Minutes 9/24/18"
date: 2018-09-24 19:00:00 -0500
categories: september minutes 2018
---
# Hacktoberfest
- Instead holding the normal meeting on October 15th, we will meet in Becker room 120.
- Hacktoberfest is a month-long event
- We are holding a seminar (open to the public) on how to participate in the event
- Posters will soon be hung up on Becker’s walls
- If you want to help, email AITP’s email

# BYOC (build your own computer)
- We are looking for a group of people to come together that (hopefully) knows some information already
- What we will be doing is:
  - Finding out what is currently wrong with the computer we have
  - Making a presentation
  - Present sometime in Fall (in Becker 122 around 6pm possibly)
  - We are gathering a list of names who are interested and a possible time to meet
- If interested contact AITP’s email

# Vex’s Robotics

- The high school finished building the robot, we should get it sometime this upcoming week
- They wish to set up a forum using google, where they would post questions and we would respond (within twenty-four hours) while we are free throughout the day
- Nothing is completely figured out, however the initial plan is,
  - Hold a workshop day on November 19th from 9:30 to 1:00 (Monday before Thanksgiving break)
  - We want to set up several stations where we split up the kids, some station ideas are:
	- Engineering notebook
	- Use raspberry pie where we can mod mondcraft
	- Code in python a robot (in becker)
    - Put together safety rules (so the kids don’t fight)
	- A possible Tour (advertise Clarion)
	- Lunch will be served
   - At each station, we will have to talk to the kids as fast or as slow as the time is allotted.
- Remember the logistics are not figured out and these all may change
- If interested send Dr. Packer an email

# AITP Website(temporary)
- We have put up a website hosted by gitlab

# The PACISE Conf.
- This has moved to Millersville (it is now further away)
- There are presentation opportunities here too

# Coding Competition
- Looking for two teams of three for a robotics team and coding team
- There are also presenters for posters and papers
- No experience needed!
- The competition is held in the Spring semester however:
  - Practices will start every Friday between 2pm and 3pm in the advanced computer lab

# Fundraising
- We do not want to sell candy (or anything generic)
- We may sell things that we can cut out with the laser cutter

# Jody’s Corner
- We NEED to start recruiting (we hopefully won’t be here forever)
  - We can have a small activity were we can get people to just hang out with us
- Sometime in November Keith will present his keyboards/drone seminar
- West PACC’S is November 10th
- October 12th a former student will come back to talk about their job, and the opportunities
- We are getting a new projector
- We have a room number G-42! And a plaque that says it!!!
- We will soon be holding small talks from interviewees 
