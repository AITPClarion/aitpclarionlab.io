---
layout: page
title: About Us
permalink: /about/
---

AITP is a registered student organization here at Clarion University. We are based out of Techfloor which is 
located in Ralston Hall, room G-42. If you get down to the basement and wander around eventually you will see signs if you don't know where it is. We, as an organization, strive to better our members through both teaching and learning oportunities.

# Meetings
We meet every Monday at 7:00PM at Techfloor. If you would like to be notified of cancellations then join the organization on [CUConnect](https://cuconnect.clarion.edu/organization/aitp)!

# Officers
#### President
Alex Day
#### Vice-President
Jon Lander
#### Treasurer
Thomas Csorba
#### Secretary
Heidi-Jo Shuttleworth
#### PR-Chair
Kayln Combetty
